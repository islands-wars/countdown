(() => {

    const id = 'PLkbZMlMxyCRDehbl2tOQX53WyaucDn11v'
    const videoCount = 5

    const player = document.querySelector('#player')
    const iframe = document.createElement("iframe")

    const parameters = [
        ['list', id],
        ['index', ~~(Math.random() * videoCount)],
        ['autoplay', 1],
        ['controls', 0],
        ['fs', 0],
        ['rel', 0],
        ['showinfo', 0],
        ['loop', 1],
        ['shuffle', 1],
        ['iv_load_policy', 3],
    ].map(option => option.join('='))

    iframe.src = `https://www.youtube.com/embed/videoseries?${parameters.join('&')}`
    player.appendChild(iframe)

})()
