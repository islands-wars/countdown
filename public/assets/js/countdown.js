(() => {

    const finalDate = new Date("Jun 28, 2018 12:12:12").getTime()

    const elements = {
        days: document.getElementById('days'),
        hours: document.getElementById('hours'),
        minutes: document.getElementById('minutes'),
        seconds: document.getElementById('seconds')
    }

    const second = 1000
    const minute = second * 60
    const hour = minute * 60
    const day = hour * 24

    setInterval(function () {

        const diff = finalDate - new Date().getTime()

        elements.days.innerHTML = ~~(diff / day)
        elements.hours.innerHTML = ~~((diff % day) / hour)
        elements.minutes.innerHTML = ~~((diff % hour) / minute)
        elements.seconds.innerHTML = ~~((diff % minute) / second)

    }, 1000)

})()
