# <img src="https://islands-wars.gitlab.io/assets/brands/icons/icon.svg" width="64"> Countdown

> Islands Wars is a multiplayer Minecraft server.

> Simple website with a countdown.

---

## Getting started

These instructions will get you a copy of the project.

### Prerequisites

You'll need to install software(s) mentionned below.
* [Git](https://www.git-scm.com) - Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency.

### Steps

#### Clone `Countdown`'s repository

```bash
$ git clone git@gitlab.com:islands-wars/assets.git
```

This command will download `Countdown`'s repository in a folder named `assets`.

#### Edit configuration

##### `public/assets/js/countdown.js`

In `countdown.js` at line `3` you can modify `finalDate` variable to change the due date.

```js
const finalDate = new Date("Jun 28, 2018 12:12:12").getTime()
```

##### `public/assets/js/youtube.js`

In `youtube.js` you can modify `id` and `videoCount` variable to change the playlist ID and number of videos inside.

```js
const id = 'PLkbZMlMxyCRDehbl2tOQX53WyaucDn11v'
const videoCount = 5
```

---

## CI/CD

* [master](https://gitlab.com/islands-wars/countdown/tree/master) - ![status](https://gitlab.com/islands-wars/countdown/badges/master/build.svg)
* [develop](https://gitlab.com/islands-wars/countdown/tree/develop) - ![status](https://gitlab.com/islands-wars/countdown/badges/develop/build.svg)

---

## License

> Countdown is MIT licensed.

---

## Authors

* [LightDiscord](https://lightdiscord.fr) - Initial work

